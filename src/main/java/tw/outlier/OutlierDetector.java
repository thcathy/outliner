package tw.outlier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tw.outlier.data.Repository;
import tw.outlier.data.parser.DataPointCSVParser;
import tw.outlier.entity.DataPoint;
import tw.outlier.entity.OutlierResult;
import tw.outlier.process.*;

import com.google.common.base.Optional;

/**
 * The outlier detector consolidate in, out and processor
 */
public class OutlierDetector 
{
	final private static Logger log = LoggerFactory.getLogger(OutlierDetector.class);
		
	final private Repository<DataPoint> dataPointRepository;
	final private DataPointOutlierDetector outlierDetector;
	final private OutlierResultProcessor resultProcessor;
	
	private OutlierResult result;
	
	public OutlierDetector(String filePath, double tolerance) {
		dataPointRepository = new DataPointCSVParser(filePath);
		outlierDetector = new StandardScoreOutlierDetector(tolerance);
		resultProcessor = new OutlierResultFormatter(System.out);
	}
	
	public void detectOutlier() {
		result = outlierDetector.removeOutliers(dataPointRepository.getAll());
		log.debug("Outlier detected. Have outlier: {}, outliers size: {}, healthy data size: {}"
				, result.isOutlierFound(), result.getOutliers().size(), result.getHealthyDataPoint().size());
		
		resultProcessor.process(result);
	}
	
	public Optional<OutlierResult> getResult() { return Optional.of(result); };
	
}
