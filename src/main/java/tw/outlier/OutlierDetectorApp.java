package tw.outlier;

import org.apache.commons.cli.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Optional;

/**
 * The main application to launch outlier detector
 * Usage: java tw.outlier.OutlierDetectorApp -f <filePath> -t <tolerance from standard deviation>
 */
public class OutlierDetectorApp 
{
	final private static Logger log = LoggerFactory.getLogger(OutlierDetectorApp.class);
	
	final private static String OPTION_FILE = "f";
	final private static String OPTION_TOLERANCE = "t";
	final private static Options OPTIONS = buildCmdOptions();
		
	public static void main( String[] args )
    {
    	Optional<OutlierDetector> detector = buildOutlierDetector(args);
    	if (detector.isPresent()) { 
    		detector.get().detectOutlier(); 
    	} else {
    		printUsage(OPTIONS);
    	}
    }
	
	private static void printUsage(Options options) {
		new HelpFormatter().printHelp(OutlierDetectorApp.class.getName(), options);
	}
	
	/**
	 * @return optional detector, absent if any exception during construction
	 */
	private static Optional<OutlierDetector> buildOutlierDetector(String[] args) {
		CommandLineParser parser = new BasicParser();    	
		 try {
		    CommandLine line = parser.parse(OPTIONS, args);
		    String filePath = line.getOptionValue(OPTION_FILE);
		    double tolerance = Double.parseDouble(line.getOptionValue(OPTION_TOLERANCE));
		    log.debug("Construct OutlierDetector with filePath: {}, tolerance: {}", filePath, tolerance);
		
			// if no file path input, return absent
			if (StringUtils.isBlank(filePath)) {
				log.error("Invalid file path: " + filePath);
		    	printUsage(OPTIONS);
		    	return Optional.absent();
		    }
		    
		    return Optional.of(new OutlierDetector(filePath, tolerance));
		} catch (ParseException e) {
		    log.error("Parsing failed. Reason: " + e.getMessage());
		} catch (NumberFormatException e) {
			log.error("Cannot parse tolerance." + e.getMessage());
		}
		return Optional.absent();
	}

    /**
     * Build command line options
     * -f --file {file path}
     * -t --tolerance {tolerance} max allow deviation
     */
	@SuppressWarnings("static-access")
	private static Options buildCmdOptions() {
		Options options = new Options();
		
		options.addOption(
    			OptionBuilder.withLongOpt("tolerance")
                	.hasArg(true).withArgName("number")
                	.withDescription("max allow deviation")
                	.create(OPTION_TOLERANCE));
		
    	options.addOption(
    			OptionBuilder.withLongOpt("file")
                	.hasArg(true).withArgName("path")
                	.withDescription("file path to parse data points")
                	.create(OPTION_FILE));
    	
    	
		return options;
	}
}
