package tw.outlier.data;

import java.util.List;

/**
 * Interface for accessing entity objects
 */
public interface Repository<T> {
	public List<T> getAll();
}
