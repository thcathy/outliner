package tw.outlier.data.parser;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tw.outlier.data.Repository;

import com.google.common.base.Optional;

/**
 * A kind of repository to retrieve line from CSV file
 * Sub class need to implement the actual construction of return entity
 */
public abstract class CSVParser<T> implements Repository<T> {
	final private static Logger log = LoggerFactory.getLogger(DataPointCSVParser.class);
	
	final public String DELIMITOR = ",";
	
	final protected String filePath;
	
	public CSVParser(String filePath) {
		this.filePath = filePath;
	}
			
	/**
	 * Parse file to elements which skip malformat lines
	 * @return list of element parse from file
	 * @throws RuntimeException if any io error occurs
	 */
	public List<T> getAll() {		
		log.debug("start parser objects from csv file {}", filePath);
		List<T> results = new ArrayList<T>();
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(filePath));
			String line = null;
			while ((line = reader.readLine()) != null) {
				Optional<T> element = buildElement(line.split(DELIMITOR));
				if (element.isPresent())
					results.add(element.get());
			}
		} catch (IOException e) {
			log.error("IO exception when parse file: " + filePath, e);
			throw new RuntimeException("Fail to parse file: " + filePath, e);
		}
		finally {			
			if (reader != null)	{
				try {reader.close();} catch (IOException e) { log.error("IOException detected", e); }			
			}
		}
		return results;		
	}

	abstract protected Optional<T> buildElement(String[] values); 
}
