package tw.outlier.data.parser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tw.outlier.entity.DataPoint;

import com.google.common.base.Optional;

/**
 * Parser to construct data point from CSV file
 */
public class DataPointCSVParser extends CSVParser<DataPoint> {
	final private static Logger log = LoggerFactory.getLogger(DataPointCSVParser.class);
	
	public DataPointCSVParser(String filePath) {
		super(filePath);
	}

	/**
	 * Construct a DataPoint. Given absent if any exception found
	 * @param values[0] date in string
	 * @param values[1] price
	 */
	@Override 	
	protected Optional<DataPoint> buildElement(String[] values) {
		try {
			return Optional.of(new DataPoint(values[0], values[1]));
		} catch (Exception e) {
			log.info("Fail to construct DataPoint with: " + values, e);
			return Optional.absent();
		}
	}
}
