package tw.outlier.entity;

public class DataPoint {
	final private String date;
	final private double price;
	final private String priceString;	// remain the actual price str from display
	
	public DataPoint(String date, String priceInString) {
		this.date = date;
		this.price = Double.parseDouble(priceInString);
		this.priceString = priceInString;			// use for display
	}
	
	public double getPrice() { return price; }
	public String getPriceString() { return priceString; }
	public String getDate() { return date; }

	@Override
	public String toString() {
		return String.format("%s, %s", date, priceString);
	}
}
