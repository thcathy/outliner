package tw.outlier.entity;

import java.util.List;

/**
 * A immutable class storing the result after remove outliers
 */
public class OutlierResult {
	final private List<DataPoint> outliers;
	final private List<DataPoint> healthyDataPoint;
			
	public OutlierResult(List<DataPoint> healthyDataPoint, List<DataPoint> outliers) {		
		this.healthyDataPoint = healthyDataPoint;
		this.outliers = outliers;
	}
	
	public boolean isOutlierFound() { return outliers.size() > 0; }
	public List<DataPoint> getOutliers() { return outliers; }
	public List<DataPoint> getHealthyDataPoint() { return healthyDataPoint;	}
	
}
