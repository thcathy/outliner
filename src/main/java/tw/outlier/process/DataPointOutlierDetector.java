package tw.outlier.process;

import java.util.List;

import tw.outlier.entity.DataPoint;
import tw.outlier.entity.OutlierResult;

/**
 * Interface for detector outlier from a list of data point
 */
public interface DataPointOutlierDetector {
	public OutlierResult removeOutliers(List<DataPoint> data);
}
