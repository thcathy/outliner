package tw.outlier.process;

import java.io.IOException;
import java.io.OutputStream;

import org.apache.commons.io.IOUtils;

import tw.outlier.entity.DataPoint;
import tw.outlier.entity.OutlierResult;

/**
 * A Outlier Result Processor print result into output stream
 */
public class OutlierResultFormatter implements OutlierResultProcessor {
	private static String LINE_SEPARATOR = System.getProperty("line.separator");
	
	private final OutputStream out;
		
	public OutlierResultFormatter(OutputStream out) {
		this.out = out;
	}
	
	/**
	 * Print the outlier result into output stream
	 * @throws RuntimeException if any io error occurs
	 */
	public void process(OutlierResult result) {
		try {
			IOUtils.write("Outlier Found: " + result.isOutlierFound() + LINE_SEPARATOR, out);
			IOUtils.write("Outliers (" + result.getOutliers().size() + "): " + LINE_SEPARATOR, out);
			for (DataPoint d : result.getOutliers()) {
				IOUtils.write("[" + d.toString() + "] ", out);
			}
			IOUtils.write(LINE_SEPARATOR, out);
			IOUtils.write("Healthy Data (" + result.getHealthyDataPoint().size() + "): " + LINE_SEPARATOR, out);
			for (DataPoint d : result.getHealthyDataPoint()) {
				IOUtils.write("[" + d.toString() + "] ", out);
			}
		} catch (IOException e) {
			throw new RuntimeException("Fail to print outlier result.", e);
		}
	}
	
}
