package tw.outlier.process;

import tw.outlier.entity.OutlierResult;

/**
 * Interface for classes which process the outlier detection result
 */
public interface OutlierResultProcessor {
	public void process(OutlierResult result);
}
