package tw.outlier.process;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tw.outlier.entity.DataPoint;
import tw.outlier.entity.OutlierResult;

/**
 * A outlier detector based on stardard score
 * statistics based on the elements of the list
 * The algorithm: remove (data-mean/standard deviation)  > tolerance
 */
public class StandardScoreOutlierDetector implements DataPointOutlierDetector {
	private static Logger log = LoggerFactory.getLogger(StandardScoreOutlierDetector.class);
	
	private final double tolerance;
		
	/**
	 * @param tolerance maximum allow deviation
	 */
	public StandardScoreOutlierDetector(double tolerance) {
		this.tolerance = tolerance;
	}
	
	/**
	 * The algorithm: The algorithm: remove (data-mean/standard deviation)  > tolerance
	 * @return list of data point without outliers
	 */
	public OutlierResult removeOutliers(List<DataPoint> data) {
		log.debug("remove outlier from data size: {} which over tolerance: {}", data.size(), tolerance);
		List<DataPoint> healthyData = new ArrayList<DataPoint>();
		List<DataPoint> outliers = new ArrayList<DataPoint>();
		double sumOfValue = 0;			// to cal mean
		double sumOfValueSq = 0;		// to cal variance
		int sampleSize = 0;
						
		for (DataPoint d : data) {
			sampleSize++;
			sumOfValue += d.getPrice();
			sumOfValueSq += d.getPrice() * d.getPrice();
			
			double standardScore = Math.abs((d.getPrice() - (sumOfValue/sampleSize)) / Math.sqrt(sumOfValueSq/sampleSize));
			log.debug("standardScore of {} is {}", d, standardScore);
			if (standardScore > tolerance) {
				outliers.add(d);
			} else {
				healthyData.add(d);
			}
		}	
				
		return new OutlierResult(healthyData, outliers);
	}

}
