package tw.outlier;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import tw.outlier.data.parser.DataPointCSVParser;
import tw.outlier.entity.DataPoint;

public class DataPointCSVParserTest {
	
	@Test(expected=RuntimeException.class) 
	public void parseNonexistFileThrowRuntimeException() throws Exception {
		DataPointCSVParser parser = new DataPointCSVParser("it is not exist");
		parser.getAll();
	}
	
	@Test
	public void parseNormalCSVWithHeader() {		
		DataPointCSVParser parser = new DataPointCSVParser(this.getClass().getClassLoader().getResource("Outliers.csv").getPath());
		List<DataPoint> results = parser.getAll();
		assertEquals("Results size should be 1000", 1000, results.size());
	}
	
	@Test
	public void parseMalFormatCSVReturnEmptyList() {
		DataPointCSVParser parser = new DataPointCSVParser(this.getClass().getClassLoader().getResource("malformat.csv").getPath());
		List<DataPoint> results = parser.getAll();
		assertEquals("Results size should be 0", 0, results.size());
	}
	
	@Test
	public void parseEmptyCSVReturnEmptyList() {
		DataPointCSVParser parser = new DataPointCSVParser(this.getClass().getClassLoader().getResource("empty.csv").getPath());
		List<DataPoint> results = parser.getAll();
		assertEquals("Results size should be 0", 0, results.size());
	}
}
