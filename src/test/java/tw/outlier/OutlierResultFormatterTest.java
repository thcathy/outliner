package tw.outlier;

import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.util.ArrayList;

import org.junit.Test;

import tw.outlier.entity.DataPoint;
import tw.outlier.entity.OutlierResult;
import tw.outlier.process.OutlierResultFormatter;

public class OutlierResultFormatterTest {
		
	@Test
	public void printEmptyOutlierResult() {
		OutputStream out = new ByteArrayOutputStream(50);
		OutlierResult emptyResult = new OutlierResult(new ArrayList<DataPoint>(), new ArrayList<DataPoint>());
				
		// process
		new OutlierResultFormatter(out).process(emptyResult);
		
		// verify
		String outputString = out.toString();
		assertTrue(outputString.startsWith("Outlier Found: false"));
		assertTrue(outputString.contains("Outliers (0):"));
		assertTrue(outputString.contains("Healthy Data (0):"));
	}
	
	@Test
	@SuppressWarnings("serial")
	public void printNormalOutlierResult() throws ParseException {
		OutputStream out = new ByteArrayOutputStream(300);		
		OutlierResult outlierResult = new OutlierResult(
			new ArrayList<DataPoint>() {{
				add(new DataPoint("20140101", "-50"));
			}},
			new ArrayList<DataPoint>() {{ 
				add(new DataPoint("20140102", "-1.03"));
				add(new DataPoint("20140103", "-456"));
			}}
		);
			
		// process
		new OutlierResultFormatter(out).process(outlierResult);
		
		// verify
		String outputString = out.toString();
		assertTrue(outputString.startsWith("Outlier Found: true"));
		assertTrue(outputString.contains("Outliers (2):"));
		assertTrue(outputString.contains("[20140102, -1.03] "));
		assertTrue(outputString.contains("[20140103, -456] "));
		assertTrue(outputString.contains("Healthy Data (1):"));
		assertTrue(outputString.contains("[20140101, -50] "));
	}
}

