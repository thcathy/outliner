package tw.outlier;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import tw.outlier.entity.DataPoint;
import tw.outlier.entity.OutlierResult;
import tw.outlier.process.DataPointOutlierDetector;
import tw.outlier.process.StandardScoreOutlierDetector;

public class StandardScoreOutlierDetectorTest {
		
	@Test
	public void noOutlierIfDataSizeIsLessThanTwo() {
		// empty data
		DataPointOutlierDetector detector = new StandardScoreOutlierDetector(0.0);
		
		// process
		OutlierResult result = detector.removeOutliers(new ArrayList<DataPoint>());
		
		// verify
		assertFalse("No outlier for empty input", result.isOutlierFound());
		assertEquals("No healthy data for empty input", 0, result.getHealthyDataPoint().size());
		assertEquals("No outlier for empty input", 0, result.getOutliers().size());
		
		// data size is one
		detector = new StandardScoreOutlierDetector(0.0);
		List<DataPoint> inputData = new ArrayList<DataPoint>();
		inputData.add(new DataPoint("20140101", "100.05"));		
		
		// process
		result = detector.removeOutliers(inputData);
		
		// verify
		assertFalse("No outlier for 1 input", result.isOutlierFound());
		assertEquals("1 healthy data for 1 input", 1, result.getHealthyDataPoint().size());
		assertEquals("No outlier for 1 input", 0, result.getOutliers().size());
	}
	
	
	@Test
	@SuppressWarnings("serial")
	public void allDataIsOutliersAsZeroTolerance() {
		DataPointOutlierDetector detector = new StandardScoreOutlierDetector(0.0);
		List<DataPoint> inputData = new ArrayList<DataPoint>() {{
			add(new DataPoint("2014/01/01", "100.1"));
			add(new DataPoint("2014/01/01", "99.9"));
			add(new DataPoint("2014/01/01", "100.5"));
			add(new DataPoint("2014/01/01", "99.5"));
		}};
		
		// process
		OutlierResult result = detector.removeOutliers(inputData);
		
		// verify
		assertTrue("Should find outlier", result.isOutlierFound());
		assertEquals("1 healthy data found", 1, result.getHealthyDataPoint().size());
		assertEquals("All data point after the first one are outliers", inputData.size() - 1, result.getOutliers().size());		
	}
	
	@Test
	@SuppressWarnings("serial")
	public void detectOneOutlier() {
		DataPointOutlierDetector detector = new StandardScoreOutlierDetector(1.2);		
		final DataPoint outlier = new DataPoint("2014/01/01", "8");
		List<DataPoint> inputData = new ArrayList<DataPoint>() {{		
			add(new DataPoint("2014/01/02", "-1"));
			add(new DataPoint("2014/01/03", "1"));
			add(new DataPoint("2014/01/04", "0.5"));
			add(outlier);
			add(new DataPoint("2014/01/05", "-3.21"));
		}};
		
		// process
		OutlierResult result = detector.removeOutliers(inputData);
		
		// verify
		assertTrue("Should find outlier", result.isOutlierFound());
		assertEquals("4 healthy data found", 4, result.getHealthyDataPoint().size());
		assertEquals("1 data point is outliers", 1, result.getOutliers().size());
		assertEquals("First element is outlier", outlier, result.getOutliers().get(0));
	}
	
	@Test
	@SuppressWarnings("serial")
	public void detectOutliersAtDiffPos() {
		DataPointOutlierDetector detector = new StandardScoreOutlierDetector(2.0);		
		final DataPoint outlier1 = new DataPoint("2014/01/02", "20");
		final DataPoint outlier2 = new DataPoint("2014/02/12", "-130");
		final DataPoint outlier3 = new DataPoint("2014/02/18", "10000");
		
		List<DataPoint> inputData = new ArrayList<DataPoint>() {{
			add(new DataPoint("2014/01/04", "0.35"));	
			add(new DataPoint("2014/01/04", "1.5"));	
			add(new DataPoint("2014/01/04", "0.0"));	
			add(new DataPoint("2014/01/04", "-0.3"));	
			add(new DataPoint("2014/01/04", "-0.1"));		
			add(outlier1);
			add(new DataPoint("2014/02/01", "1"));
			add(new DataPoint("2014/02/02", "0.5"));
			add(new DataPoint("2014/02/03", "-0.21"));
			add(outlier2);
			add(outlier3);
		}};
		
		// process
		OutlierResult result = detector.removeOutliers(inputData);
		
		// verify
		assertTrue("Should find outlier", result.isOutlierFound());
		assertEquals("8 healthy data found", 8, result.getHealthyDataPoint().size());
		assertEquals("3 data point is outliers", 3, result.getOutliers().size());
		assertEquals(outlier1, result.getOutliers().get(0));
		assertEquals(outlier2, result.getOutliers().get(1));
		assertEquals(outlier3, result.getOutliers().get(2));
	}
}
