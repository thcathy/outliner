package tw.outlier.integration;

import static org.junit.Assert.*;

import org.junit.Test;

import tw.outlier.OutlierDetector;
import tw.outlier.entity.OutlierResult;

public class OutlierDetectorIntegrationTest {
	@Test
	public void testWithCSVFile() {
		OutlierDetector detector = new OutlierDetector(this.getClass().getClassLoader().getResource("Outliers.csv").getPath(), 0.2);
		
		// process
		detector.detectOutlier();
		
		// verify
		OutlierResult result =  detector.getResult().get();
		assertTrue("Should found outlier", result.isOutlierFound());
		assertEquals("Outliers size is 342", 342, result.getOutliers().size());
		assertEquals("The first outlier price is 124.855201", "124.855201", result.getOutliers().get(0).getPriceString());
		assertEquals("Healthy data size is 658", 658, result.getHealthyDataPoint().size());
		assertEquals("The last healthy data price is 133.4512731", "133.4512731", result.getHealthyDataPoint().get(657).getPriceString());
	}
}
